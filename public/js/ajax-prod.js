$(document).ready(function(){
    var url = "/productcreate";

    var url2 = "/productedit";

    var prod_data = [];
    //create new task / update existing task
    $("#product_sub").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault();

        var formData = {
            name: $('#name').val(),
            qty: $('#qty').val(),
            price: $('#price').val(),
        }

        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();

        var type = "POST"; //for creating new resource
        var my_url = url;

        if (state == "update"){
            type = "PUT"; //for updating existing resource
            //my_url = url2 + '/' + task_id;
        }

        //console.log(formData);

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                //console.log(data);
                //var tableData = '';

                $.each(data, function(index,jsonObject){
                    //console.log(jsonObject.name);
                    if(jsonObject.name !== undefined) {
                        $('#prod' + jsonObject.name).remove();
                        var tableData = '<tr id="prod' + jsonObject.name + '"><td>' + jsonObject.name + '</td><td>' + jsonObject.qty + '</td><td>' + jsonObject.price + '</td><td>' + jsonObject.datecreated + '</td><td>' + jsonObject.total + '</td>';
                        prod_data.push(jsonObject.name+'|'+jsonObject.qty+'|'+jsonObject.price);
                        tableData += '<td><button class="editbut" id="editprod_'+jsonObject.name+'">Edit</button>';
                        $("#table-list").append( tableData );
                    }
                    else {
                        $('#totalrow').remove();
                        tableData = '<tr id="totalrow"><td colspan="3">Total: </td><td>'+jsonObject.total+'</td>';
                        $("#table-list").append( tableData );
                    }
                });
                //$("#table-list").html( tableData );

                /*if (state == "add"){ //if user added a new record
                    $('#table-list').append(tableData);
                }*/
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    $(document).on("click", ".editbut", function(){
        prod_name = this.id.split('_');
        for (i=0;i<prod_data.length; i++) {
            var data = prod_data[i].split('|');
            //console.log(prod_name);
            if(prod_name[1]==data[0]) {
                //console.log(data[0]);
                $('input[name="name"]').val(data[0]);
                $('input[name="qty"]').val(data[1]);
                $('input[name="price"]').val(data[2]);
            }

           
        }
    })
});
