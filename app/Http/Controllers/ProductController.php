<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Routing\ResponseFactory;

class ProductController extends Controller
{
    public function index(Request $request) {
        return view('products.product');
    }


    public function savecreate(Request $request, Filesystem $filesystem, ResponseFactory $responseFactory) {
        $products = ['name' => $request->name, 'qty' => $request->qty, 'price' => $request->price, 'datecreated' => date('Y-m-d H:i:s')];
        $prodList = '';
        if($filesystem->exists('product-data.json')) {
            $json_data = json_decode($filesystem->get('product-data.json'));
            $insert = true;
            foreach($json_data->products as &$prod) {
                if(strtolower($prod->name)==strtolower($request->name)) {
                    $insert = false;
                    $prod->qty = $request->qty;
                    $prod->price = $request->price;
                }
            }
            if($insert) {
                array_push($json_data->products,$products);
            }
        }
        else {
            $json_data = ['products' => []];
            array_push($json_data['products'],$products);
        }

        $filesystem->put('product-data.json',json_encode($json_data));
        $prodList = json_decode(json_encode($json_data));

        $response_json = [];
        $total = 0;
        foreach ($prodList->products as $prod) {
            $prod1 = $prod;
            $prod1->total = intval($prod->qty) * intval($prod->price);
            $total += $prod1->total;
            array_push($response_json,$prod1);
        }
        $totalArr = ['total'=> $total];
        array_push($response_json,$totalArr);
        return $responseFactory->json($response_json);
    }
}