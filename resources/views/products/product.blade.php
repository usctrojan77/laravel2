@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">

    <!-- New Task Form -->
        <form action="" method="POST" class="form-horizontal">
        {{ csrf_field() }}


            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Product name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Quantity in stock</label>

                <div class="col-sm-6">
                    <input type="text" name="qty" id="qty" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Price per item</label>

                <div class="col-sm-6">
                    <input type="text" name="price" id="price" class="form-control">
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="button" class="btn btn-default" id="product_sub">
                        <i class="fa fa-plus"></i> Add Product
                    </button>
                </div>
            </div>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th>Product name</th>
                <th>Quantity in stock</th>
                <th>Price per item</th>
                <th>Datetime submitted</th>
                <th>Total value number</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody id="table-list" name="table-list">

            </tbody>
        </table>
    </div>

    <!-- TODO: Current Tasks -->
@endsection